import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, Image, Alert, TextInput } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';

//import consola from 'consola';
//const consola = require('consola');

// Camera And Image Picker
import * as ImagePicker from 'expo-image-picker';
// Permissions
import * as Permissions from 'expo-permissions';
// FileSystem access
import * as FileSystem from 'expo-file-system';
// SQLite
import * as SQLite from 'expo-sqlite';

// Redux and state managment
import { Provider, useSelector, useDispatch } from 'react-redux';
import { ACTIONS } from './redux/actions.js';
import store from './redux/store';

// WSV -- Wide Scoped Variables 
const devmode = true;

function FirstAppChild () {
	// State variables not worth putting into redux
	const [mutateBtnLbl, setMutateBtnLbl] = React.useState('UPDATE');
	const [fname, setFName] = React.useState('');
	const [lname, setLName] = React.useState('');
	const [addrs1, setAddrs1] = React.useState('');
	const [addrs2, setAddrs2] = React.useState('');
	const [cty, setCty] = React.useState('');
	const [postcd, setPostcd] = React.useState('');
	const [email, setEmail] = React.useState('');
	const [phone, setPhone] = React.useState('');
	const [country, setCountry] = React.useState('');
	const [state, setState] = React.useState('');
	const [countryIsSet, setCountryIsSet] = React.useState(false);
	const [stateIsSet, setStateIsSet] = React.useState(false);
	// Redux setup
	const appState = useSelector(state => state);
	const dispatch = useDispatch();

	// Database
	const db = SQLite.openDatabase('contactsDB');

	const states = [{label:"Ontario", value:{}}, {label: "PEI", value:{}}];
	const countries = [{label:"Canada", value:{}}, {label: "America", value:{}}];

	let ExpectedImagePath =  FileSystem.documentDirectory + 'profile.jpg'; 
	async function SaveProfilePicture (){
		// move file found at appState.profilePic to documentDirectory
		try {
			await FileSystem.moveAsync({
				from: 	appState.profilePic,
				to:		ExpectedImagePath	
			});
			dispatch(ACTIONS.setProfilePic( ExpectedImagePath));
			Alert.alert('Image saved at', ExpectedImagePath);
		}
		catch (err) {
			console.log(`Error moving Profile Picture --> ${err}`);
			throw err; // fail obviously and fail imedietly
		}
	}

	async function TakeProfilePicture(){
		const options = {
			allowsEditing: true,
		};
		let photoEventData = await ImagePicker.launchCameraAsync(options);
		let path = photoEventData[`uri`];
		dispatch(ACTIONS.setProfilePic(path));
	}

	React.useEffect (() => {
		//-----------------------------------------------------------------------------------------------
		async function CheckAndLoadProfilePicture () {
			// Check to see if data already excists
			const finfo = await FileSystem.getInfoAsync(ExpectedImagePath);
			// If it does excist --> it should be loaded into the app
			if (finfo.exists)
				dispatch(ACTIONS.setProfilePic(ExpectedImagePath));
			else 
				Alert.alert('Did not find image', `${ExpectedImagePath}\n\n${JSON.stringify(finfo)}`);
		}
		CheckAndLoadProfilePicture();
		//-----------------------------------------------------------------------------------------------
		// Get data from the database 
		db.transaction(
			tx => {
				tx.executeSql(
					'select * from contacts',
					[],
					(_, { rows } ) => {
						console.log('ROW(s) RETRIEVED!');
						rows._array.map(row => {
							console.log(`\t${row}`);
							setFName(row.firstname);
							setLName(row.lastname);
							setAddrs1(row.addressline1);
							setAddrs2(row.addressline2);
						//	setPostcd(row.pcode);
							setCty(row.city);
							{
								setCountry(row.country);
								setCountryIsSet(true);
							}
							{
								setState(row.state);
								setStateIsSet(true);
							}
							setEmail(row.email);
							setPhone(row.phone);

						});
					},
					(_, result) => {
						console.log('SELECT failed');
					}
				)
			}
		);
		
	}, []);

	return (
		<View style={styles.container}>
			<Text>w_doyle88383</Text>
			<Text>Contact Info App</Text>

			<TouchableOpacity style={styles.profileImageContainer} onPress={() => TakeProfilePicture()}>
				<Image style={styles.profileImage} source={{uri: appState.profilePic,}}/>
			</TouchableOpacity>
			<View style={styles.DataInterface}>
				{/*FIRST AND LAST NAME*/}
				<View style={styles.vrow}>
					<View style={styles.vcolSMALL}>
						<Text>First Name</Text>
						<TextInput 
							value={fname}
							placeholder={`First Name`} 
							onChangeText={val => setFName(val)}  
							style={styles.InputText}/>
					</View>
					<View style={styles.vcolSMALL}>
						<Text>Last Name</Text>
						<TextInput
							value={lname}
							placeholder={`Last Name`} 
							onChangeText={val => setLName(val)}  
							style={styles.InputText}/>
					</View>
				</View>

				{/*ADDRESS*/}
				<View style={styles.vcol}>
					<Text>Address</Text>
					<TextInput 
							value={addrs1}
						placeholder={`Address Line 1`}
						onChangeText={val => setAddrs1(val)}  
						style={styles.InputText}/>
					<TextInput 
							value={addrs2}
						placeholder={`Address Line 2`}  
						onChangeText={val => setAddrs2(val)}  
						style={styles.InputTextTOPSPACE}/>
				</View>
				<View style={styles.vrow}>
					<View style={styles.vcolSMALL}>
						<Text>City</Text>
						<TextInput 
							value={cty}
							placeholder={`City`} 
							onChangeText={val => setCty(val)}  
							style={styles.InputText}/>
						<Text>Province/State</Text>
						<DropDownPicker 
							style={styles.InputDropDown} 
							containerStyle={{height:40, width: '100%'}} 
							dropDownStyle={{marginTop: 2}}  
							items={states} 
							defaultIndex={0} 
							placeholder="Province/State"
							onChangeItem={item => {
								setState(item.label);
								setStateIsSet(item);
								//	Alert.alert(JSON.stringify(item));
							}}
						/>

					</View>
					<View style={styles.vcolSMALL}>
						<Text>Postal Code</Text>
						<TextInput 
							value={postcd}
							placeholder={`Postal Code`}
							onChangeText={val => setPostcd(val)}  
							style={styles.InputText}/>
						<Text>Country</Text>
						<DropDownPicker 
							style={styles.InputDropDown} 
							containerStyle={{height:40, width: '100%'}} 
							dropDownStyle={{marginTop: 2}}  
							items={countries} 
							defaultIndex={0} 
							placeholder="Country"
							onChangeItem={item => {
								setCountry(item.label);
								setCountryIsSet(true);
								//	Alert.alert(JSON.stringify(item));
							}}
						/>

					</View>

				</View>

				{/*CONTACT DETAILS*/}
				<View style={styles.vcol}>
					<Text>Email Address</Text>
					<TextInput 
						value={email}
						placeholder={`Email Address`} 
						onChangeText={val => setEmail(val)}  
						style={styles.InputText}/>
					<Text>Phone Number</Text>
					<TextInput 
						value={phone}
						placeholder={`Phone Number`} 
						onChangeText={val => setPhone(val)}  
						style={styles.InputText}/>
				</View>


			</View>

			<Button style={styles.mutateBtn} title={`${mutateBtnLbl}`} 
				onPress={() => {
					// Validate Inputs
					function empty(s) {
						return s === '';
					}
					// Validate Email
					function isEmail(es){
						var res = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
						return !res.test(String(es).toLowerCase());
					}
					// Validate phone number
					function isPhone(ps){
						return /^\d+$/.test(ps);
					}

					{	//	V A L I D A T E   I N P U T S
						if(	empty(fname))
						{Alert.alert(`\"${fname}\" invalid value for first name `); return;}
						if(	empty(lname))
						{Alert.alert(`\"${lname}\" invalid value for last name `); return;}
						if(	empty(addrs1))
						{Alert.alert(`\"${addrs1}\" invalid value for Address Line 1`); return;}
						if(empty(cty))
						{Alert.alert(`\"${cty}\" invalid value for City`); return;}
						if(empty(postcd))
						{Alert.alert(`\"${postcd}\" invalid value for Postal Code`); return;}
						if(isEmail(email))
						{Alert.alert(`\"${email}\" invalid value for email`); return;}
						if(!isPhone(phone))
						{Alert.alert(`\"${phone}\" invalid value for phone number`); return;}
						if (!countryIsSet){
							Alert.alert('You must select a country');
							return;
						}
						if (!stateIsSet){
							Alert.alert('You must select a province/state');
							return;
						}
					}

					// move profile picture to permanent storage in the documentDirectory
					SaveProfilePicture();

					// Save Contact Details to database
					// C R E A T E   T A B L E   I F   N E E D E D
					db.transaction(tx => {
						tx.executeSql(`create table if not exists contacts (
								id				integer primary key not null,
								firstname		text,
								lastname		text,
								addressline1	text,
								addressline2	text,
								city			text,
								country			text,
								state			text,
								email			text,
								phone			text
						);`,
							[],
							() => console.log('TABLE CREATED'),
							(_/* gross */, result) => console.log('TABLE CREATE failed: ' + result) 
						);
					});

					// I N S E R T   C O N T A C T
					db.transaction(
						tx => {
							tx.executeSql(
								`insert into contacts (firstname, lastname, addressline1, addressline2, city, country, state, email, phone) values (?,?,?,?,?,?,?,?,?)`, 
								[fname, lname, addrs1, addrs2, cty, country, state, email, phone],
								(_, { rowsAffected }) => rowsAffected > 0 ? console.log(`${rowsAffected} ROW(S) inserted`):console.log(`INSERT FAILED`),
								(_, result) => console.log('INSERT failed: ' + result)
							);
						}
					);


					setMutateBtnLbl((mutateBtnLbl === 'UPDATE')? 'SAVE':'UPDATE');
				}} />

			{
				devmode && <Button title={'DROP'} onPress={() => {
					console.log('about to attempt drop');
					db.transaction(tx => {
						tx.executeSql(`drop table contacts`,[], ()=>console.log('DROPED TABLE'), (_, result) => console.log('FAILD TO DROP TABLE' + result ));
					});

					Alert.alert('Dropoing Data');

				}}/>
			}
			<StatusBar style="auto" />
		</View>
	);

}

export default function App() {
	return (
		<View style={styles.container}>
			<Provider store={store}>
				<FirstAppChild/>
			</Provider>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	profileImageContainer: {
		borderWidth: 1,
		borderRadius: 10,	
	},
	profileImage: {
		width: 120,
		height: 120,
	},
	mutateBtn: {
	},
	DataInterface: {
		padding: 3,
		margin: 10,
		backgroundColor: '#EEF',
		borderColor: '#CCF',
		borderWidth: 1,
		borderRadius: 10,
		padding: 7,
	},
	InputText: {
		borderWidth: 1,
		borderRadius: 5,
		padding: 7,
	},
	InputTextTOPSPACE: {
		marginTop: 10,
		borderWidth: 1,
		borderRadius: 5,
		padding: 7,
	},
	vrow: {
		flexDirection: "row",
		width: 250,
	},
	vcol: {
		padding: 5,
		flexDirection: "column",
		width: 250,
	},
	vcolSMALL: {
		padding: 5,
		flexDirection: "column",
		width: 125,
	},

	InputDropDown: {
	},
});
