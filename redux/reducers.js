const initialState = {
	profilePic: "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.knack.com%2Fimages%2Fabout%2Fdefault-profile.png&f=1&nofb=1",
}

export function appReducer (state = initialState, action) {
	switch (action.type){
		case 'SETPROFILEPIC':
			return {
				...state,
				profilePic: action.profilePic
			}

		default:
			return state;
	}
}
